Document: jaf-api
Title: Debian jaf-api Manual
Author: <insert document author here>
Abstract: This manual describes what jaf-api is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/jaf-api/jaf-api.sgml.gz

Format: postscript
Files: /usr/share/doc/jaf-api/jaf-api.ps.gz

Format: text
Files: /usr/share/doc/jaf-api/jaf-api.text.gz

Format: HTML
Index: /usr/share/doc/jaf-api/html/index.html
Files: /usr/share/doc/jaf-api/html/*.html
